
# React boilerplate++
So at first this was going to be a full fledged replacement for the spreadsheet based MMA contest on sa.
It ended up being more of a learning experience for using react with the new ES2015 features.  I'm thinking 
of leaving it as is since my interest in MMA has waned and I'm thinking of moving onto something more 
interesting now that I've figured out some concepts here that I can leave for reference.

Basically I've got a login that calls out to a stub function and loads a different component on successful login.
Nothing fancy but still uncovered a lot of gotchas I wasn't expecting.

### Usage

```
npm install
npm start
Open http://localhost:5000
```

### Linting

```
npm run lint
```

### acknowledgements
vasanthk's boilerplate (github.com/vasanthk/react-es6-webpack-boilerplate)

eXon's react-cookie (https://github.com/eXon/react-cookie)

skeleton css (getskeleton.com)
