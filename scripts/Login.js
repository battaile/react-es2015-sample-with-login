import React, {Component} from 'react/addons';
import cookie from 'react-cookie';
import * as auth from './authenticate';

export default class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {user: cookie.load('user') };
	};
	handleChange(event) {
		let userId = event.target.value;
		this.setState({user: userId});
		cookie.save('user', userId);
	};
	handleClick(){
		if (auth.authenticate(this.state.user)){
			this.props.userChanged(this.state.user);
		} else {
			this.setState({errormsg: 'User not found.'});
		}
	};
	render() {
		return (
			<form>
				<div className="row">
					<div className="five columns">
						<label htmlFor="userText">User Name </label>
						<input className="u-full-width" type="text" id="userText"
								placeholder="User Name"
								value={this.state.user}
								onChange={this.handleChange.bind(this)} />
						<span className="error">{this.state.errormsg}</span>
					</div>
				</div>
				<div className="row">
					<input type="button" name="loginButton" value="Let's Go!" onClick={this.handleClick.bind(this)}/>
				</div>
			</form>
		);
	}
}
