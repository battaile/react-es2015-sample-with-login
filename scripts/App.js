import React, {Component} from 'react';
import Login from './Login';
import ContestStatus from './ContestStatus';
import '../css/normalize.css';
import '../css/skeleton.css';
import '../css/site.css';

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = { user: ''};
	};

	onUserChange(user){
		this.setState({user: user});
		this.render();
	}

  render() {
    let authenticated = this.state.user !== '';
    return (
		<div className="container">
			{authenticated && <ContestStatus /> || <Login userChanged = {this.onUserChange.bind(this)} /> }
		</div>
	);
  }
}
